import argparse
import glob
#import mimetypes
import re

RESERVED_WORDS = ['break', 'case', 'catch', 'class', 'const', 'continue', 'debugger', 'default', 'delete', 'do', 'else', 'export', 'extends', 'false', 'finally', 'for', 'function', 'if', 'import', 'in', 'instanceof', 'new', 'null', 'return', 'super', 'switch', 'this', 'throw', 'true', 'try', 'typeof', 'var', 'void', 'while', 'with', 'yield']

def extract_variables(javascript_file, min_length):
    # Check the MIME type of the file
    # mime_type = mimetypes.guess_type(javascript_file)[0]
    # if mime_type not in ('application/javascript', 'text/javascript'):
    #     print(f'Skipping file {javascript_file}: Not a JavaScript file')
    #     return []

    # Read the contents of the JavaScript file
    try:
        with open(javascript_file, 'r', encoding='utf-8') as f:
            contents = f.read()
    except:
        contents = ''

    # Use a regular expression to find all variable declarations
    variable_declarations = re.findall(r'(?:var|const|let)\s+(\b[^=\s;]+\b)(?=\s*[=;])(?!\s*(?:function|if|for|while))', contents)

    # Return the list of unique variable names of variable length and are not reserved JavaScript words 
    return [variable for variable in set(variable_declarations) if len(variable) >= min_length and variable.isalnum() and variable not in RESERVED_WORDS]

if __name__ == '__main__':
    # Parse the command-line arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--file', required=True, nargs='+', help='The JavaScript file or files to process')
    parser.add_argument('-s', '--separator', default=', ', help='Output separator')
    parser.add_argument('-l', '--min-length', type=int, default=2, help='Minimum variable length to extract')
    args = parser.parse_args()

    # Extract the variables from each JavaScript file
    variables = []
    for f in args.file:
        variables.extend(extract_variables(f, args.min_length))

    # Print the list of variables separated by a comma
    print(args.separator.join(variables))

