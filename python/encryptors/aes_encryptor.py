# write a shellcode aes encryptor in python that takes in raw binary shellcode file as argument and outputs encrypted shellcode in c# format. use a pseudo-random decryption key each time without using the secrets library. use argparse instead of sys and use multiline strings instead of multiple prints.

import argparse
import random
from Cryptodome.Cipher import AES

# Parse the command line arguments
parser = argparse.ArgumentParser()
parser.add_argument("shellcode_file", help="Path to the file containing the shellcode")
args = parser.parse_args()

# Read the shellcode from the file
with open(args.shellcode_file, "rb") as f:
    shellcode = f.read()

# Generate a pseudo-random 16-byte key for AES encryption
key = bytearray(random.getrandbits(8) for _ in range(16))

# Pad the shellcode to a multiple of 16 bytes
padding_length = 16 - (len(shellcode) % 16)
shellcode += b'\x00' * padding_length

# Create the AES cipher object and encrypt the shellcode
cipher = AES.new(key, AES.MODE_ECB)
encrypted_shellcode = cipher.encrypt(shellcode)

# Output the encrypted shellcode in C# format
print("byte[] key = new byte[] { " + ", ".join("0x" + key.hex()[i:i+2] for i in range(0, len(key.hex()), 2)) + " };")
print("byte[] encryptedShellcode = new byte[] { " + ", ".join("0x" + encrypted_shellcode.hex()[i:i+2] for i in range(0, len(encrypted_shellcode.hex()), 2)) + " };")

