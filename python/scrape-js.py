import argparse
import os
import urllib.parse
import urllib.request

from selenium import webdriver


def main():
    # Parse the command-line arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("url", help="URL of the web page to load")
    args = parser.parse_args()
    url = args.url

    # Set up headless Chrome options
    options = webdriver.ChromeOptions()
    options.add_argument("--headless")

    # Create a Chrome browser instance
    driver = webdriver.Chrome(options=options)

    try:
        # Navigate to the web page
        driver.get(url)

        # Wait for the page to finish loading
        driver.implicitly_wait(10)

        # Get a list of all JavaScript resources on the page
        scripts = driver.execute_script(
            """
            return Array.from(document.querySelectorAll("script")).map(s => s.src);
            """
        )

        # Get a list of all inline JavaScript resources on the page
        inline_scripts = driver.execute_script(
            """
            return Array.from(document.querySelectorAll("script:not([src])")).map(s => s.textContent);
            """
        )

        # Keep track of the filenames that have been used
        used_filenames = set()

        # Iterate through the list of scripts and save each one locally
        for i, script in enumerate(scripts):
            try:
                # Download the script
                response = urllib.request.urlopen(script)

                # Remove any HTTP GET parameters from the filename
                _, _, path, _, _, _ = urllib.parse.urlparse(script)
                filename = os.path.basename(path)

                # Strip anything else that comes after the .js extension
                filename = filename.split(".js")[0]
                # Append a number to the filename if it is a duplicate
                if filename in used_filenames:
                    filename = f"{filename}_{i + 1}"

                # Add the filename to the set of used filenames
                used_filenames.add(filename)

                # Save the script to a local file
                with open(f"{filename}.js", "wb") as f:
                    f.write(response.read())

                # Print output
                print(f"Saved {filename}.js")
            except Exception as e:
                #print(f"Error: {e}")
                pass

        # Iterate through the list of inline scripts and save each one locally
        for i, script in enumerate(inline_scripts):
            # Save the inline script to a local file
            filename = f"inline_script{i + 1}"
            with open(f"{filename}.js", "w") as f:
                f.write(script)

            # Print output
            print(f"Saved {filename}.js")
    except Exception as e:
        #print(f"Error: {e}")
        pass
    finally:
        # Close the browser
        driver.quit()


if __name__ == "__main__":
    main()

